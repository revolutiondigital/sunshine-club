from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django import forms
from cms.models import CMSPlugin

class EventsAndNewsChinese(models.Model):
	name = models.CharField(max_length=200)
	event_start=  models.CharField(max_length=200,blank=True,null=True,verbose_name="event-start- if its news, don't add")
	event_end=  models.CharField(max_length=200,blank=True,null=True,verbose_name="event end- if its news, don't add")
	date_posted=  models.CharField(max_length=200)
	event_link=  models.CharField(max_length=200)
	content = models.TextField()
	event_image = models.ImageField(upload_to="uploads/events")
	date_added = models.DateTimeField(auto_now=True,auto_now_add=True)
	def __unicode__(self):
		return self.name