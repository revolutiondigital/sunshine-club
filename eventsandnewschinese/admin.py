from django.contrib import admin
from eventsandnewschinese.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class EventsAndNewsChineseAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('name', 'event_start','event_end', 'date_posted','content')
admin.site.register(EventsAndNewsChinese, EventsAndNewsChineseAdmin)
