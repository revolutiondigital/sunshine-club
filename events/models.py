from django.db import models
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django import forms
from cms.models import CMSPlugin

class EventAnd(models.Model):
	name = models.CharField(max_length=200)
	event_start=  models.CharField(max_length=200,blank=True,null=True)
	event_end=  models.CharField(max_length=200,blank=True,null=True)
	date_posted=  models.CharField(max_length=200)
	event_link=  models.CharField(max_length=200)
	content = models.TextField()
	event_image = models.ImageField(upload_to="uploads/testimonialpic", verbose_name=u'picture must be 160 x 90(square) pixels')
	def __unicode__(self):
		return self.name

class EventPluginModel(CMSPlugin):
    event = models.ForeignKey('events.Event', related_name='plugins')

    def __unicode__(self):
      return self.event.name