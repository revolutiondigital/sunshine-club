# Create your views here.
# Create your views here.
# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader

from events.models import Event

def index(request):
    event = Event.objects.filter()
    template = loader.get_template('event.html')
    context = RequestContext(request, {
        'event': event,
    })
    return HttpResponse(template.render(context))
# Create your views here.
