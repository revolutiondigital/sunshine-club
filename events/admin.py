from django.contrib import admin
from events.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class EventAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('name', 'event_start','event_end', 'content')
admin.site.register(Event, EventAdmin)
