from django.contrib import admin
from eventsandnewsenglish.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class EventsAndNewsEnglishAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('name', 'event_start','event_end', 'date_posted','content')
admin.site.register(EventsAndNewsEnglish, EventsAndNewsEnglishAdmin)
