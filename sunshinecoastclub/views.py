from django.http import HttpResponse
import os
import settings
from django.views.decorators.cache import never_cache
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from eventsandnewsenglish.models import *
from eventsandnewschinese.models import *
from django.template.loader import render_to_string
def get_chinese_events_main(request):
	import sys;
	reload(sys);
	sys.setdefaultencoding("utf8")
	values = EventsAndNewsChinese.objects.filter().order_by('-date_added')[:5]
	rendered = render_to_string('news-events-front-chinese.html', {'news_events': values}).encode('utf8')
	return HttpResponse(rendered)


def get_english_events_main(request):
	values = EventsAndNewsEnglish.objects.filter().order_by('-date_added')[:5]
	rendered = render_to_string('news-events-front-english.html', {'news_events': values}).encode('utf8')
	return HttpResponse(rendered)	


def get_chinese_events_grid(request):
	values = EventsAndNewsChinese.objects.filter().order_by('-date_added')
	rendered = render_to_string('news-and-events-page-chinese.html', {'news_events': values}).encode('utf8')
	return HttpResponse(rendered)


def get_english_events_grid(request):
	values = EventsAndNewsEnglish.objects.filter().order_by('-date_added')
	rendered = render_to_string('news-and-events-page-english.html', {'news_events': values}).encode('utf8')
	return HttpResponse(rendered)	