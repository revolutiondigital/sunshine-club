from django.contrib import admin
from testimonial.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class TestimonialAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('name', 'content')
admin.site.register(Testimonial, TestimonialAdmin)
