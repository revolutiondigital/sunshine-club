from django.db import models
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django import forms
from cms.models import CMSPlugin

class Testimonial(models.Model):
	name = models.CharField(max_length=200)
	content = models.TextField()
	profileimage = models.ImageField(upload_to="uploads/testimonialpic", verbose_name=u'picture must be 100 x 100(square) pixels')
	def __unicode__(self):
		return self.name

class TestimonialPluginModel(CMSPlugin):
    testimonial = models.ForeignKey('testimonial.Testimonial', related_name='plugin')

    def __unicode__(self):
      return self.testimonial.name