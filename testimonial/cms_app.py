# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

class TestimonialApp(CMSApp):
    name = _("Testimonial App")        # give your app a name, this is required
    urls = ["testimonial.urls"]       # link your app to url configuration(s)
    app_name = "testimonial"          # this is the application namespace

apphook_pool.register(TestimonialApp) # register your app