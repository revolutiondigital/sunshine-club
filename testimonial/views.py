# Create your views here.
# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader

from testimonial.models import Testimonial

def index(request):
    testimonial = Testimonial.objects.filter()
    template = loader.get_template('testimonial.html')
    context = RequestContext(request, {
        'testimonial': testimonial,
    })
    return HttpResponse(template.render(context))
# Create your views here.
